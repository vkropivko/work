package com.test.work.model


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("analytics_response_payload")
    val analyticsResponsePayload: String, // e=Z2lmX2lkPTNvNlp0NDgxaXNOVnVRSTFsNiZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPWUxYmI3MmZmaHhhdzRuYzkyeXZidTR5Y253ZnZnaHZpMzhvZnYxd2x1MG9ycXZoNw
    @SerializedName("bitly_gif_url")
    val bitlyGifUrl: String, // http://gph.is/2jfHZM5
    @SerializedName("bitly_url")
    val bitlyUrl: String, // http://gph.is/2jfHZM5
    @SerializedName("content_url")
    val contentUrl: String,
    @SerializedName("embed_url")
    val embedUrl: String, // https://giphy.com/embed/3o6Zt481isNVuQI1l6
    @SerializedName("id")
    val id: String, // 3o6Zt481isNVuQI1l6
    @SerializedName("images")
    val images: Images,
    @SerializedName("import_datetime")
    val importDatetime: String, // 2017-01-07 22:08:56
    @SerializedName("is_sticker")
    val isSticker: Int, // 0
    @SerializedName("rating")
    val rating: String, // pg-13
    @SerializedName("slug")
    val slug: String, // cat-smoke-smoking-3o6Zt481isNVuQI1l6
    @SerializedName("source")
    val source: String, // https://www.facebook.com/sheepfilms/posts/1312160308841893
    @SerializedName("source_post_url")
    val sourcePostUrl: String, // https://www.facebook.com/sheepfilms/posts/1312160308841893
    @SerializedName("source_tld")
    val sourceTld: String, // www.facebook.com
    @SerializedName("title")
    val title: String, // cat smoking GIF by sheepfilms
    @SerializedName("trending_datetime")
    val trendingDatetime: String, // 2017-06-23 10:30:01
    @SerializedName("type")
    val type: String, // gif
    @SerializedName("url")
    val url: String, // https://giphy.com/gifs/cat-smoke-smoking-3o6Zt481isNVuQI1l6
    @SerializedName("username")
    val username: String // sheepfilms
)