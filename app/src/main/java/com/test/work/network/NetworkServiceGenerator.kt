package com.test.work.network

import io.reactivex.schedulers.Schedulers
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class NetworkServiceGenerator(endpoint: String) {

    val netApi: NetApi

    init {
        val builderHttp = OkHttpClient.Builder()
        builderHttp.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val originalHttpUrl: HttpUrl = original.url
            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", "dc6zaTOxFJmzC")
                .build()
            val requestBuilder: Request.Builder = original.newBuilder()
                .url(url)
            chain.proceed(requestBuilder.build())
        })
        builderHttp.addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        val builder = Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(builderHttp.build())
            .build();
        netApi = builder.create(NetApi::class.java);
    }
}