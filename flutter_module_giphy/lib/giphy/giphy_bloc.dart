import 'package:flutter_module_giphy/base/base_bloc.dart';
import 'package:flutter_module_giphy/giphy/giphy_event.dart';
import 'package:flutter_module_giphy/giphy/giphy_repository.dart';
import 'package:flutter_module_giphy/giphy/search_giphy_response.dart';

import 'giphy_state.dart';

class GiphyBloc extends BaseBloc<BaseGiphyEvent, GiphyState> {
  var _repository = GiphyRepositoryImp();
  PaginationBean _pagination;

  @override
  BaseGiphyEvent get initEvent => BaseGiphyEvent();

  GiphyBloc() : super(initialState: GiphyState("", []));

  @override
  Stream<GiphyState> mapEventToState(BaseGiphyEvent event) async* {
    if (event is ClickLoadGiphyEvent) {
      _pagination = PaginationBean(count: 16, offset: 0, totalCount: 0);
      var result =
          await _repository.searchGif(event.search, _pagination.count, 0);
      yield GiphyState(event.search, result.getListUrls());
    } else if (event is LoadMoreGiphyEvent) {
      var currentState = state;
      _pagination.next();
      var result = await _repository.searchGif(
          currentState.search, _pagination.count, _pagination.offset);
      var list = currentState.listUrls;
      list.addAll(result.getListUrls());
      _pagination = result.pagination;
      yield GiphyState(currentState.search, list);
    }
  }
}
