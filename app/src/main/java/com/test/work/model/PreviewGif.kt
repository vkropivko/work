package com.test.work.model


import com.google.gson.annotations.SerializedName

data class PreviewGif(
    @SerializedName("height")
    val height: String, // 89
    @SerializedName("size")
    val size: String, // 48436
    @SerializedName("url")
    val url: String, // https://media1.giphy.com/media/3o6Zt481isNVuQI1l6/giphy-preview.gif?cid=e1bb72ffhxaw4nc92yvbu4ycnwfvghvi38ofv1wlu0orqvh7&rid=giphy-preview.gif
    @SerializedName("width")
    val width: String // 142
)