import 'dart:convert';
import 'package:flutter_module_giphy/base/base_bloc.dart';
import 'package:http/http.dart' as http;

class HttpNetworkConfiguration {
  final String baseUrl;

  HttpNetworkConfiguration(this.baseUrl);

  Future<GiphyResponse> sendGet(
      String path, Map<String, String> queryParameters) async {
    var url = createUrlWithParams(baseUrl, path, queryParameters);
    http.Response response =
        await http.get(url);
    var result = GiphyResponse(response.statusCode, response.body);
    logger.d("HTTP_URL_"+url);
    logger.d("HTTP_BODY_"+result.body);
    return result;
  }

  String createUrlWithParams(
      String baseUrl, String path, Map<String, String> queryParameters) {
    var result = StringBuffer();
    result.write(baseUrl);
    result.write(path);
    if (queryParameters != null && queryParameters.length > 0) {
      result.write("?");
    }
    String separator = "";
    queryParameters.forEach((key, value) {
      if (key != null && value != null) {
        result.write(separator);
        separator = "&";
        result.write(Uri.encodeQueryComponent(key));
        result.write("=");
        result.write(Uri.encodeQueryComponent(value));
      }
    });
    return result.toString();
  }
}

class GiphyResponse {
  int statusCode;
  String body;

  dynamic get json => jsonDecode(body);

  GiphyResponse(this.statusCode, this.body);
}
