package com.test.work.model


import com.google.gson.annotations.SerializedName

data class Meta(
    @SerializedName("msg")
    val msg: String, // OK
    @SerializedName("response_id")
    val responseId: String, // hxaw4nc92yvbu4ycnwfvghvi38ofv1wlu0orqvh7
    @SerializedName("status")
    val status: Int // 200
)