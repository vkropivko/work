package com.test.work.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class GifsAdapter : RecyclerView.Adapter<GifsAdapter.GifsViewHolder>() {

    private val listUrl = mutableListOf<String>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GifsViewHolder {
        return GifsViewHolder(GifView(parent.context))
    }

    override fun onBindViewHolder(holder: GifsViewHolder, position: Int) {
        holder.view.url = listUrl[position]
    }

    override fun getItemCount(): Int {
        return listUrl.size
    }

    fun addGifs(urls: List<String>) {
        listUrl.addAll(urls)
        notifyDataSetChanged()
    }

    fun clear() {
        listUrl.clear()
        notifyDataSetChanged()
    }

    class GifsViewHolder(val view: GifView) : RecyclerView.ViewHolder(view) {

    }
}