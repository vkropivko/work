package com.test.work.network

import com.test.work.model.GiphyResponce
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NetApi {

    @GET("v1/gifs/search")
    fun getGiphky(
        @Query("q") search: String?,
        @Query("limit") limit: Any, @Query("offset") offseet: Int
    ): Single<GiphyResponce>
}