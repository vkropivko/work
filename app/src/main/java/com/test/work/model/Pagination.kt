package com.test.work.model


import com.google.gson.annotations.SerializedName

data class Pagination(
    @SerializedName("count")
    val count: Int, // 5
    @SerializedName("offset")
    var offset: Int, // 0
    @SerializedName("total_count")
    val totalCount: Int // 34187
)