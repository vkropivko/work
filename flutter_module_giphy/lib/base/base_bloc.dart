import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';

Logger logger = Logger(printer: PrettyPrinter());

abstract class BaseBloc<Event, State> extends Bloc<Event, State> {
  Event get initEvent => null;

  BaseBloc({State initialState}) : super(initialState) {
    if (initEvent != null) {
      add(initEvent);
    }
  }

  @override
  void onError(Object e, StackTrace s) {
    super.onError(e, s);
    logger.e("Bloc_error_", e);
  }
}
