class BaseGiphyEvent {}

class ClickLoadGiphyEvent extends BaseGiphyEvent {
  final String search;

  ClickLoadGiphyEvent(this.search);

  @override
  String toString() {
    return 'ClickLoadGiphyEvent{search: $search}';
  }
}

class LoadMoreGiphyEvent extends BaseGiphyEvent {

}
