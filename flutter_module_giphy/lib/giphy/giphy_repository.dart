import 'package:flutter_module_giphy/base/network_api.dart';
import 'package:flutter_module_giphy/giphy/search_giphy_response.dart';


class GiphyRepositoryImp  {

  var _netWorkApi = GiphyNetWorkApi();

  Future<SearchGiphyResponse> searchGif(String text, int limit, int offSet) {
    return _netWorkApi.searchGifs(text, limit, offSet);
  }

}
