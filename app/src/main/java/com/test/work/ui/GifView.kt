package com.test.work.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.test.work.R
import com.test.work.utils.loadImage
import kotlinx.android.synthetic.main.view_gif.view.*

class GifView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    var url: String = ""
        set(value) {
            ivGif.loadImage(value)
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_gif, this)
    }
}