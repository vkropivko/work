import 'package:flutter_module_giphy/base/network/HttpNetworkConfiguration.dart';
import 'package:flutter_module_giphy/giphy/search_giphy_response.dart';

class GiphyNetWorkApi {
  final _configuration = HttpNetworkConfiguration("https://api.giphy.com/");

  Future<SearchGiphyResponse> searchGifs(String text, int limit, int offSet) async {
    GiphyResponse result = await _configuration.sendGet("v1/gifs/search",
        {"q": text, "limit": limit.toString(),
          "offset": offSet.toString(),"api_key":"dc6zaTOxFJmzC"});
    SearchGiphyResponse response = SearchGiphyResponse.fromJson(result.json);
    return response;
  }
}
