package com.test.work.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProviders

abstract class BaseFragment<T : BaseViewModel> : Fragment() {

    lateinit var viewModel: T
    abstract val layoutResId: Int

    open fun observeLiveData() {

    }

    abstract val viewModelClass: Class<T>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        observeBaseLiveData()
        observeLiveData()
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel()
    }

    private fun observeBaseLiveData() {
        viewModel.progressError.observe {
            Toast.makeText(activity ?: return@observe, it, Toast.LENGTH_LONG).show()
        }
    }

    fun <T> LiveData<T>.observe(observer: (T) -> Unit) {
        this.observe(this@BaseFragment.viewLifecycleOwner, {
            if (it != null) observer(it)
        })
    }

    fun createViewModel(): T {
        return ViewModelProviders.of(this).get(viewModelClass)
    }
}