class SearchGiphyResponse {
  PaginationBean pagination;
  List<DataListBean> data;

  SearchGiphyResponse({this.pagination, this.data});

  SearchGiphyResponse.fromJson(Map<String, dynamic> json) {
    this.pagination = json['pagination'] != null
        ? PaginationBean.fromJson(json['pagination'])
        : [null];
    this.data = (json['data'] as List) != null
        ? (json['data'] as List).map((i) => DataListBean.fromJson(i)).toList()
        : [];
  }

  List<String> getListUrls(){
    return data.map((e) => e.images.previewGif.url).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pagination != null) {
      data['pagination'] = this.pagination.toJson();
    }
    data['data'] =
        this.data != null ? this.data.map((i) => i.toJson()).toList() : null;
    return data;
  }
}

class PaginationBean {
  int count;
  int offset;
  int totalCount;

  PaginationBean({this.count, this.offset, this.totalCount});

  PaginationBean.fromJson(Map<String, dynamic> json) {
    this.count = json['count'];
    this.offset = json['offset'];
    this.totalCount = json['total_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    data['offset'] = this.offset;
    data['total_count'] = this.totalCount;
    return data;
  }

  void next() {
    offset = offset + count;
  }
}

class DataListBean {
  String analyticsResponsePayload;
  String bitlyGifUrl;
  String bitlyUrl;
  String contentUrl;
  String embedUrl;
  String id;
  String importDatetime;
  String rating;
  String slug;
  String source;
  String sourcePostUrl;
  String sourceTld;
  String title;
  String trendingDatetime;
  String type;
  String url;
  String username;
  int isSticker;
  ImagesBean images;

  DataListBean(
      {this.analyticsResponsePayload,
      this.bitlyGifUrl,
      this.bitlyUrl,
      this.contentUrl,
      this.embedUrl,
      this.id,
      this.importDatetime,
      this.rating,
      this.slug,
      this.source,
      this.sourcePostUrl,
      this.sourceTld,
      this.title,
      this.trendingDatetime,
      this.type,
      this.url,
      this.username,
      this.isSticker,
      this.images});

  DataListBean.fromJson(Map<String, dynamic> json) {
    this.analyticsResponsePayload = json['analytics_response_payload'];
    this.bitlyGifUrl = json['bitly_gif_url'];
    this.bitlyUrl = json['bitly_url'];
    this.contentUrl = json['content_url'];
    this.embedUrl = json['embed_url'];
    this.id = json['id'];
    this.importDatetime = json['import_datetime'];
    this.rating = json['rating'];
    this.slug = json['slug'];
    this.source = json['source'];
    this.sourcePostUrl = json['source_post_url'];
    this.sourceTld = json['source_tld'];
    this.title = json['title'];
    this.trendingDatetime = json['trending_datetime'];
    this.type = json['type'];
    this.url = json['url'];
    this.username = json['username'];
    this.isSticker = json['is_sticker'];
    this.images =
        json['images'] != null ? ImagesBean.fromJson(json['images']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['analytics_response_payload'] = this.analyticsResponsePayload;
    data['bitly_gif_url'] = this.bitlyGifUrl;
    data['bitly_url'] = this.bitlyUrl;
    data['content_url'] = this.contentUrl;
    data['embed_url'] = this.embedUrl;
    data['id'] = this.id;
    data['import_datetime'] = this.importDatetime;
    data['rating'] = this.rating;
    data['slug'] = this.slug;
    data['source'] = this.source;
    data['source_post_url'] = this.sourcePostUrl;
    data['source_tld'] = this.sourceTld;
    data['title'] = this.title;
    data['trending_datetime'] = this.trendingDatetime;
    data['type'] = this.type;
    data['url'] = this.url;
    data['username'] = this.username;
    data['is_sticker'] = this.isSticker;
    if (this.images != null) {
      data['images'] = this.images.toJson();
    }
    return data;
  }
}

class ImagesBean {
  PreviewGifBean previewGif;

  ImagesBean({this.previewGif});

  ImagesBean.fromJson(Map<String, dynamic> json) {
    this.previewGif = json['preview_gif'] != null
        ? PreviewGifBean.fromJson(json['preview_gif'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.previewGif != null) {
      data['preview_gif'] = this.previewGif.toJson();
    }
    return data;
  }
}

class PreviewGifBean {
  String height;
  String size;
  String url;
  String width;

  PreviewGifBean({this.height, this.size, this.url, this.width});

  PreviewGifBean.fromJson(Map<String, dynamic> json) {
    this.height = json['height'];
    this.size = json['size'];
    this.url = json['url'];
    this.width = json['width'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['height'] = this.height;
    data['size'] = this.size;
    data['url'] = this.url;
    data['width'] = this.width;
    return data;
  }
}
