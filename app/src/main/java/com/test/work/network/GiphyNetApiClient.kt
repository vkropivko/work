package com.test.work.network

import com.test.work.model.GiphyResponce
import io.reactivex.Single

class GiphyNetApiClient(endPoint: String) {

    private val netApi: NetApi = NetworkServiceGenerator(endPoint).netApi

    fun searchGiphy(search: String, limit: Int, offSet: Int): Single<GiphyResponce> {
        return netApi.getGiphky(search, limit, offSet)
    }

}