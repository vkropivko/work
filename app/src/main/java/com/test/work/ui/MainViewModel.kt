package com.test.work.ui

import com.test.work.model.Pagination
import com.test.work.ui.main.GiphyRepository
import com.test.work.ui.main.GiphyRepositoryImpl
import com.test.work.utils.SingleLiveData


class MainViewModel(private val repositoryImpl: GiphyRepository = GiphyRepositoryImpl()) :
    BaseViewModel() {

    private var searchText: String = ""
    val gifsData = SingleLiveData<List<String>>()
    val clearGifsData = SingleLiveData<Unit>()
    private var pagination = Pagination(16, 0, 0)


    fun loadGIfs(search: String) {
        repositoryImpl.searchGiphy(search, pagination.count, pagination.offset)
            .startRequest({
                clearGifsData.postValue(Unit)
                pagination = it.pagination
                gifsData.postValue(it.gifsUrls)
            })
    }

    fun onScrollEnd() {
        repositoryImpl.searchGiphy(
            searchText,
            pagination.count,
            pagination.offset + pagination.count
        )
            .startRequest({
                pagination = it.pagination
                gifsData.postValue(it.gifsUrls)
            })
    }
}