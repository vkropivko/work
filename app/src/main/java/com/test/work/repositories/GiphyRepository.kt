package com.test.work.ui.main

import com.test.work.model.GiphyResponce
import com.test.work.network.GiphyNetApiClient
import io.reactivex.Single

interface GiphyRepository {

    fun searchGiphy(search: String, limit: Int, offSet: Int): Single<GiphyResponce>
}

class GiphyRepositoryImpl : GiphyRepository {


    private val networkApi = GiphyNetApiClient("https://api.giphy.com")

    override fun searchGiphy(search: String, limit: Int, offSet: Int): Single<GiphyResponce> {
        return networkApi.searchGiphy(search, limit, offSet)
    }

}


