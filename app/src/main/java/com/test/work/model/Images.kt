package com.test.work.model


import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("preview_gif")
    val previewGif: PreviewGif,
)