package com.test.work.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.work.utils.SingleLiveData
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

open class BaseViewModel : ViewModel() {

    protected val comDisposable = CompositeDisposable()
    val progressData = SingleLiveData<Boolean>()
    val progressError = SingleLiveData<String>()

    fun <T> Single<T>.startRequest(
        onSuccess: ((T) -> Unit), progress: SingleLiveData<Boolean> = progressData
    ) {
        ioToMain()
            .doOnSubscribe { progressData.postValue(true) }
            .doOnDispose { progressData.postValue(false) }
            .subscribe(createConsumer(onSuccess, progress))
    }

    override fun onCleared() {
        super.onCleared()
        comDisposable.clear()
    }


    private fun <T> createConsumer(
        onSuccess: ((T) -> Unit),
        progressLiveData: MutableLiveData<Boolean>,
        bindToViewModelLifecycle: Boolean = true
    ) = object :
        SingleObserver<T> {
        override fun onSuccess(result: T) {
            progressLiveData.value = false
            onSuccess(result)
        }

        override fun onSubscribe(disposable: Disposable) {
            if (bindToViewModelLifecycle) {
                comDisposable += disposable
            }
            progressLiveData.postValue(true)
        }

        override fun onError(throwable: Throwable) {
            progressLiveData.value = false
            parseError(throwable)

        }
    }

    private fun parseError(throwable: Throwable) {
        /*Какой-то парс ошибки и тд и стрелять в liveData*/
        if(throwable is HttpException){
            progressError.postValue("Допустим 500 ошибка")
        }else {
            progressError.postValue("Допустим апи умерло")
        }
    }


}

/*Что бы не плодить утильные классы вынес сюда*/
infix operator fun CompositeDisposable.plusAssign(subscribe: Disposable) {
    this.add(subscribe)
}

fun <T> Single<T>.ioToMain(): Single<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}