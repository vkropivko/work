package com.test.work.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.test.work.R
import com.test.work.utils.ScrollListenerAdapter
import kotlinx.android.synthetic.main.main_fragment.*


class MainFragment : BaseFragment<MainViewModel>() {

    override val layoutResId = R.layout.main_fragment
    override val viewModelClass = MainViewModel::class.java
    private val adapter by lazy { GifsAdapter() }

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun observeLiveData() {
        viewModel.gifsData.observe {
            adapter.addGifs(it)
        }
        viewModel.clearGifsData.observe {
            adapter.clear()
        }
        viewModel.progressData.observe {
            pbMoreLoad.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()

    }

    private fun initView() {
        rvGifs.adapter = adapter
        rvGifs.addOnScrollListener(object : ScrollListenerAdapter() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!rvGifs.canScrollVertically(RecyclerView.FOCUS_DOWN) && adapter.itemCount > 0) {
                    Log.d("blss_","end_scroll")
                    viewModel.onScrollEnd()
                }
            }
        })
        bSearch.setOnClickListener {
            viewModel.loadGIfs(etSearch.text.toString())
        }
    }
}