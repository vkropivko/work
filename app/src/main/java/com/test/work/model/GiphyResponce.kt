package com.test.work.model


import com.google.gson.annotations.SerializedName

data class GiphyResponce(
    @SerializedName("data")
    private val _data: List<Data>,
    @SerializedName("meta")
    private val meta: Meta,
    @SerializedName("pagination")
    val pagination: Pagination
) {

    val gifsUrls: List<String>
        get() = _data.map { it.images.previewGif.url }
}