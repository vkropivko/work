import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_module_giphy/base/base_bloc.dart';
import 'package:flutter_module_giphy/giphy/giphy_bloc.dart';
import 'package:flutter_module_giphy/giphy/giphy_event.dart';
import 'package:flutter_module_giphy/giphy/giphy_state.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (BuildContext context) => GiphyBloc(),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _textController = TextEditingController();

  ScrollController _scrollController = ScrollController();

  double _currentScroll;

  GiphyBloc bloc;

  @override
  Widget build(BuildContext context) {
    /*По хорошему тут можно написать своб обертку над BlocBuilder что бы приходил еще 3 параметром bloc и не пришлось его каждый раз брать */
    return BlocBuilder<GiphyBloc, GiphyState>(builder: (context, state) {
      var urls = state.listUrls ?? [];
      logger.d("build_list_size_" + urls.length.toString());
      _textController.text = (state.search);
      return Scaffold(
        appBar: AppBar(
          title: Text("work.ua"),
        ),
        bottomNavigationBar: MaterialButton(
          child: Text("Найти"),
          textColor: Colors.white,
          color: Colors.blue,
          height: 48,
          onPressed: () {
            BlocProvider.of<GiphyBloc>(context)
                .add(ClickLoadGiphyEvent(_textController.text));
          },
        ),
        body: Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: _textController,
                  decoration: InputDecoration(labelText: "поиск"),
                ),
              ),
              Expanded(
                child: GridView.count(
                  crossAxisCount: 2,
                  controller: _scrollController,
                  children: urls
                      .map((e) => Card(
                              child: Image.network(
                            e,
                            fit: BoxFit.fill,
                          )))
                      .toList(),
                ),
              )
            ],
          ),
        ),
      );
    });
  }

  void initListener(BuildContext context) {
    _scrollController.addListener(() {
      if (_currentScroll == _scrollController.position.pixels) {
        return;
      }
      final maxScroll = _scrollController.position.maxScrollExtent;
      _currentScroll = _scrollController.position.pixels;
      if (maxScroll == _currentScroll) {
        bloc.add(LoadMoreGiphyEvent());
      }
    });
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<GiphyBloc>(context);
    initListener(context);
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    bloc.close();
  }
}
